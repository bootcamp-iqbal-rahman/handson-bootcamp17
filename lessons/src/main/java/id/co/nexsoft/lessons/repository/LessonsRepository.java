package id.co.nexsoft.lessons.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.lessons.model.Lessons;
import jakarta.transaction.Transactional;

public interface LessonsRepository extends JpaRepository<Lessons, Long> {
    @Query("SELECT l FROM Lessons l")
    List<Lessons> getAllData();

    @Query("SELECT l FROM Lessons l WHERE l.id = :id")
    Lessons getDataById(Long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Lessons WHERE id = :ids")
    void deleteById(Long ids);

    @Transactional
    @Modifying
    @Query("INSERT INTO Lessons (subject, class_number) VALUES (:#{#data.subject}, :#{#data.class_number})")
    void addData(@Param("data") Lessons data);

    @Transactional
    @Modifying
    @Query("UPDATE Lessons SET subject = :#{#data.subject}, class_number = :#{#data.class_number} WHERE id = :id")
    void updateData(@Param("data") Lessons data, Long id);

}
