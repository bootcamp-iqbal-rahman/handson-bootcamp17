package id.co.nexsoft.lessons.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.lessons.model.Teacher;
import id.co.nexsoft.lessons.repository.TeacherRepository;
import id.co.nexsoft.lessons.service.DefaultService;
import id.co.nexsoft.lessons.service.DynamicService;
import id.co.nexsoft.lessons.service.PojoService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class TeacherServiceImpl implements DefaultService<Teacher>, DynamicService<Teacher>, PojoService<Teacher> {
    @Autowired
    private TeacherRepository repository;

    @Autowired
    private EntityManager entityManager;
    
    @Override
    public List<Teacher> getAllData() {
        return repository.getAllData();
    }

    @Override
    public Teacher getDataById(Long id) {
        return repository.getDataById(id);
    }

    @Override
    public void addData(Teacher data) {
        repository.save(data);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateData(Teacher data, Long id) {
        data.setId(id);
        repository.save(data);
    }

    @Transactional
    @Override
    public void updateData(Long id, Map<String, Object> data) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE teacher SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }
}