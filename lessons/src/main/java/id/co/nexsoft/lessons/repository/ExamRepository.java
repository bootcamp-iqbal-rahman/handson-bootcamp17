package id.co.nexsoft.lessons.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.lessons.model.Exam;
import jakarta.transaction.Transactional;

public interface ExamRepository extends JpaRepository<Exam, Long> {
    @Query("SELECT l FROM Exam l")
    List<Exam> getAllData();

    @Query("SELECT l FROM Exam l WHERE l.id = :id")
    Exam getDataById(Long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Exam WHERE id = :ids")
    void deleteById(Long ids);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO exam (exam_date, exam_time, lesson_id) VALUES (:#{#data['exam_date']}, :#{#data['exam_time']}, :#{#data['lesson_id']})", nativeQuery = true)
    void addData(@Param("data") Map<String, Object> data);

    @Transactional
    @Modifying
    @Query(value = "UPDATE exam SET exam_date = :#{#data['exam_date']}, exam_time = :#{#data['exam_time']}, lesson_id = :#{#data['lesson_id']} WHERE id = :id", nativeQuery = true)
    void updateData(@Param("data") Map<String, Object> data, @Param("id") Long id);

}
