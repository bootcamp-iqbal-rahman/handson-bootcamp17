package id.co.nexsoft.lessons.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.lessons.model.StudentExamScore;
import id.co.nexsoft.lessons.repository.StudentExamScoreRepository;
import id.co.nexsoft.lessons.service.CustomService;
import id.co.nexsoft.lessons.service.DefaultService;
import id.co.nexsoft.lessons.service.DynamicService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class StudentExamScoreServiceImpl implements DefaultService<StudentExamScore>, DynamicService<StudentExamScore>, CustomService<StudentExamScore> {
    @Autowired
    private StudentExamScoreRepository repository;

    @Autowired
    private EntityManager entityManager;
    
    @Override
    public List<StudentExamScore> getAllData() {
        return repository.getAllData();
    }

    @Override
    public StudentExamScore getDataById(Long id) {
        return repository.getDataById(id);
    }

    @Override
    public void addData(Map<String, Object> data) {
        repository.addData(data);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateData(Map<String, Object> data, Long id) {
        repository.updateData(data, id);
    }

    @Transactional
    @Override
    public void updateData(Long id, Map<String, Object> data) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE student_exam_score SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public void addData(StudentExamScore data) {
        repository.save(data);
    }
}