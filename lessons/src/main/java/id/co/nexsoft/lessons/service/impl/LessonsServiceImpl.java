package id.co.nexsoft.lessons.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.lessons.model.Lessons;
import id.co.nexsoft.lessons.repository.LessonsRepository;
import id.co.nexsoft.lessons.service.DefaultService;
import id.co.nexsoft.lessons.service.PojoService;

@Service
public class LessonsServiceImpl implements DefaultService<Lessons>, PojoService<Lessons> {
    @Autowired
    private LessonsRepository repository;
    
    @Override
    public List<Lessons> getAllData() {
        return repository.getAllData();
    }

    @Override
    public Lessons getDataById(Long id) {
        return repository.getDataById(id);
    }

    @Override
    public void addData(Lessons data) {
        repository.addData(data);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateData(Lessons data, Long id) {
        repository.updateData(data, id);
    }
    
}
