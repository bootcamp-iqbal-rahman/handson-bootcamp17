package id.co.nexsoft.lessons.service;

import java.util.Map;

public interface DynamicService<T> {
    void updateData(Long id, Map<String, Object> data);
}
