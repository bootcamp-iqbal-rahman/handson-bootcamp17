package id.co.nexsoft.lessons.service;

import java.util.Map;

public interface CustomService<T> {
    void addData(Map<String, Object> data);
    void updateData(Map<String, Object> data, Long id);
}
