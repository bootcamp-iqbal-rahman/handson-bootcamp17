package id.co.nexsoft.lessons.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.lessons.model.TeacherLessonSchedule;
import jakarta.transaction.Transactional;

public interface TeacherLessonScheduleRepository extends JpaRepository<TeacherLessonSchedule, Long> {
    @Query("SELECT l FROM TeacherLessonSchedule l")
    List<TeacherLessonSchedule> getAllData();

    @Query("SELECT l FROM TeacherLessonSchedule l WHERE l.id = :id")
    TeacherLessonSchedule getDataById(Long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM TeacherLessonSchedule WHERE id = :ids")
    void deleteById(Long ids);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO teacher_lesson_schedule (teacher_id, lesson_id, teaching_date, teaching_time) VALUES (:#{#data['teacher_id']}, :#{#data['lesson_id']}, :#{#data['teaching_date']}, :#{#data['teaching_timee']})", nativeQuery = true)
    void addData(@Param("data") Map<String, Object> data);

    @Transactional
    @Modifying
    @Query(value = "UPDATE teacher_lesson_schedule SET teacher_id = :#{#data['teacher_id']}, lesson_id = :#{#data['lesson_id']}, teaching_date = :#{#data['teaching_date']}, teaching_time = :#{#data['teaching_time']} WHERE id = :id", nativeQuery = true)
    void updateData(@Param("data") Map<String, Object> data, @Param("id") Long id);

}
