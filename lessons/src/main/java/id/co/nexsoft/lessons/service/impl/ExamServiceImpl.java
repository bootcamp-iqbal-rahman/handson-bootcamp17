package id.co.nexsoft.lessons.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.lessons.model.Exam;
import id.co.nexsoft.lessons.repository.ExamRepository;
import id.co.nexsoft.lessons.service.CustomService;
import id.co.nexsoft.lessons.service.DefaultService;

@Service
public class ExamServiceImpl implements DefaultService<Exam>, CustomService<Exam> {
    @Autowired
    private ExamRepository repository;
    
    @Override
    public List<Exam> getAllData() {
        return repository.getAllData();
    }

    @Override
    public Exam getDataById(Long id) {
        return repository.getDataById(id);
    }

    @Override
    public void addData(Exam data) {
        repository.save(data);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateData(Map<String, Object> data, Long id) {
        repository.updateData(data, id);
    }

    @Override
    public void addData(Map<String, Object> data) {
        repository.addData(data);
    }
}
