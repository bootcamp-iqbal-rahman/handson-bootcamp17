package id.co.nexsoft.lessons.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.lessons.model.StudentExamScore;
import jakarta.transaction.Transactional;

public interface StudentExamScoreRepository extends JpaRepository<StudentExamScore, Long> {
    @Query("SELECT l FROM StudentExamScore l")
    List<StudentExamScore> getAllData();

    @Query("SELECT l FROM StudentExamScore l WHERE l.id = :id")
    StudentExamScore getDataById(Long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM StudentExamScore WHERE id = :ids")
    void deleteById(Long ids);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO student_exam_score (score, exam_id, student_id) VALUES (:#{#data['score']}, :#{#data['exam_id']}, :#{#data['student_id']})", nativeQuery = true)
    void addData(@Param("data") Map<String, Object> data);

    @Transactional
    @Modifying
    @Query(value = "UPDATE student_exam_score SET score = :#{#data['score']}, exam_id = :#{#data['exam_id']}, student_id = :#{#data['student_id']} WHERE id = :id", nativeQuery = true)
    void updateData(@Param("data") Map<String, Object> data, @Param("id") Long id);

}
