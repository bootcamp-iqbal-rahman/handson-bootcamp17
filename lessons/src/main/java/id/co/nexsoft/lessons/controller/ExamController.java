package id.co.nexsoft.lessons.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.lessons.model.Exam;
import id.co.nexsoft.lessons.service.CustomService;
import id.co.nexsoft.lessons.service.DefaultService;

@RestController
@RequestMapping("/api/exam")
public class ExamController {
    @Autowired
    private DefaultService<Exam> defaultService;

    @Autowired
    private CustomService<Exam> customService;

    @GetMapping
    public List<Exam> getAllData() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public Exam getDataById(@PathVariable Long id) {
        return defaultService.getDataById(id);
    }

    @PostMapping
    public void addData(@RequestBody Map<String, Object> data) {
        customService.addData(data);
    }

    @PutMapping("/{id}")
    public void updateData(@RequestBody Map<String, Object> data, @PathVariable Long id) {
        customService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        defaultService.deleteById(id);
    }
}