package id.co.nexsoft.lessons.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.lessons.model.TeacherLessonSchedule;
import id.co.nexsoft.lessons.service.CustomService;
import id.co.nexsoft.lessons.service.DefaultService;
import id.co.nexsoft.lessons.service.DynamicService;

import org.springframework.web.bind.annotation.PutMapping;


@RestController
@RequestMapping("/api/teacher/lesson")
public class TeacherLessonScheduleController {
    @Autowired
    private DefaultService<TeacherLessonSchedule> defaultService;

    @Autowired
    private DynamicService<TeacherLessonSchedule> dynamicService;

    @Autowired
    private CustomService<TeacherLessonSchedule> customService;

    @GetMapping
    public List<TeacherLessonSchedule> getAllData() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public TeacherLessonSchedule getDataById(@PathVariable Long id) {
        return defaultService.getDataById(id);
    }

    @PostMapping
    public void addData(@RequestBody Map<String, Object> data) {
        customService.addData(data);
    }

    @PutMapping("/{id}")
    public void updateData(@PathVariable Long id, @RequestBody Map<String, Object> data) {
        customService.updateData(data, id);
    }

    @PatchMapping("/{id}")
    public void updateData(@RequestBody Map<String, Object> data, @PathVariable Long id) {
        dynamicService.updateData(id, data);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        defaultService.deleteById(id);
    }
}