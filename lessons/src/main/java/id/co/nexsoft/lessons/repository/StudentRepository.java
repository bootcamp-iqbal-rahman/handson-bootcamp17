package id.co.nexsoft.lessons.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import id.co.nexsoft.lessons.model.Student;
import jakarta.transaction.Transactional;

public interface StudentRepository extends JpaRepository<Student, Long> {
    @Query("SELECT l FROM Student l")
    List<Student> getAllData();

    @Query("SELECT l FROM Student l WHERE l.id = :id")
    Student getDataById(Long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Student WHERE id = :ids")
    void deleteById(Long ids);
}
