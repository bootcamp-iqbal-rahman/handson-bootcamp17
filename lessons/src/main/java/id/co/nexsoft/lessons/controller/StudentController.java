package id.co.nexsoft.lessons.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.lessons.model.Student;
import id.co.nexsoft.lessons.service.DefaultService;
import id.co.nexsoft.lessons.service.DynamicService;
import id.co.nexsoft.lessons.service.PojoService;

import org.springframework.web.bind.annotation.PutMapping;


@RestController
@RequestMapping("/api/student")
public class StudentController {
    @Autowired
    private DefaultService<Student> defaultService;

    @Autowired
    private DynamicService<Student> dynamicService;

    @Autowired
    private PojoService<Student> pojoService;

    @GetMapping
    public List<Student> getAllData() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public Student getDataById(@PathVariable Long id) {
        return defaultService.getDataById(id);
    }

    @PostMapping
    public void addData(@RequestBody Student data) {
        defaultService.addData(data);
    }

    @PutMapping("/{id}")
    public void updateDatas(@PathVariable Long id, @RequestBody Student data) {
        pojoService.updateData(data, id);
    }

    @PatchMapping("/{id}")
    public void updateData(@RequestBody Map<String, Object> data, @PathVariable Long id) {
        dynamicService.updateData(id, data);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        defaultService.deleteById(id);
    }
}