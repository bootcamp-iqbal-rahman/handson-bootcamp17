package id.co.nexsoft.lessons.service;

import java.util.List;

public interface DefaultService<T> {
    List<T> getAllData();
    T getDataById(Long id);
    void deleteById(Long id);
    void addData(T data);
}
