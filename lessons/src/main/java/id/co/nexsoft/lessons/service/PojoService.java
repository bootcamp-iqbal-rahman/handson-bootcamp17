package id.co.nexsoft.lessons.service;

public interface PojoService<T> {
    void updateData(T data, Long id);
}
