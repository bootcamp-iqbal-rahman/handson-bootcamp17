package id.co.nexsoft.lessons.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import id.co.nexsoft.lessons.model.Teacher;
import jakarta.transaction.Transactional;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
    @Query("SELECT l FROM Teacher l")
    List<Teacher> getAllData();

    @Query("SELECT l FROM Teacher l WHERE l.id = :id")
    Teacher getDataById(Long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Teacher WHERE id = :ids")
    void deleteById(Long ids);
}
