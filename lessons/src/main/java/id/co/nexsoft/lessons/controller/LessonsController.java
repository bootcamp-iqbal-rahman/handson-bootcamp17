package id.co.nexsoft.lessons.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.lessons.model.Lessons;
import id.co.nexsoft.lessons.service.DefaultService;
import id.co.nexsoft.lessons.service.PojoService;

@RestController
@RequestMapping("/api/lessons")
public class LessonsController {
    @Autowired
    private DefaultService<Lessons> defaultService;
    
    @Autowired
    private PojoService<Lessons> pojoService;

    @GetMapping
    public List<Lessons> getAllData() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public Lessons getDataById(@PathVariable Long id) {
        return defaultService.getDataById(id);
    }

    @PostMapping
    public void addData(@RequestBody Lessons data) {
        defaultService.addData(data);
    }

    @PutMapping("/{id}")
    public void updateData(@RequestBody Lessons data, @PathVariable Long id) {
        pojoService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        defaultService.deleteById(id);
    }

    
}
