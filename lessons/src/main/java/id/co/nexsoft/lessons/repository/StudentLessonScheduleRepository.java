package id.co.nexsoft.lessons.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.nexsoft.lessons.model.StudentLessonSchedule;
import jakarta.transaction.Transactional;

public interface StudentLessonScheduleRepository extends JpaRepository<StudentLessonSchedule, Long> {
    @Query("SELECT l FROM StudentLessonSchedule l")
    List<StudentLessonSchedule> getAllData();

    @Query("SELECT l FROM StudentLessonSchedule l WHERE l.id = :id")
    StudentLessonSchedule getDataById(Long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM StudentLessonSchedule WHERE id = :ids")
    void deleteById(Long ids);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO student_lesson_schedule (student_id, lesson_id, lesson_date, lesson_time) VALUES (:#{#data['student_id']}, :#{#data['lesson_id']}, :#{#data['lesson_date']}, :#{#data['lesson_time']})", nativeQuery = true)
    void addData(@Param("data") Map<String, Object> data);

    @Transactional
    @Modifying
    @Query(value = "UPDATE student_lesson_schedule SET student_id = :#{#data['student_id']}, lesson_id = :#{#data['lesson_id']}, lesson_date = :#{#data['lesson_date']}, lesson_time = :#{#data['lesson_time']} WHERE id = :id", nativeQuery = true)
    void updateData(@Param("data") Map<String, Object> data, @Param("id") Long id);

}
