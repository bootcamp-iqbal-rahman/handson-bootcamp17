package id.co.nexsoft.lessons.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.lessons.model.Student;
import id.co.nexsoft.lessons.repository.StudentRepository;
import id.co.nexsoft.lessons.service.DefaultService;
import id.co.nexsoft.lessons.service.DynamicService;
import id.co.nexsoft.lessons.service.PojoService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Service
public class StudentServiceImpl implements DefaultService<Student>, DynamicService<Student>, PojoService<Student> {
    @Autowired
    private StudentRepository repository;

    @Autowired
    private EntityManager entityManager;
    
    @Override
    public List<Student> getAllData() {
        return repository.getAllData();
    }

    @Override
    public Student getDataById(Long id) {
        return repository.getDataById(id);
    }

    @Override
    public void addData(Student data) {
        repository.save(data);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateData(Student data, Long id) {
        data.setId(id);
        repository.save(data);
    }

    @Transactional
    @Override
    public void updateData(Long id, Map<String, Object> data) {
        StringBuilder queryBuilder = new StringBuilder("UPDATE Student SET ");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            queryBuilder.append(entry.getKey()).append(" = :").append(entry.getKey()).append(", ");
        }

        queryBuilder.delete(queryBuilder.length() - 2, queryBuilder.length());
        queryBuilder.append(" WHERE id = :id");

        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setParameter("id", id);
        query.executeUpdate();
    }
}