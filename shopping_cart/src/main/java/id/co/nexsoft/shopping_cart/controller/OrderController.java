package id.co.nexsoft.shopping_cart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.shopping_cart.model.Order;
import id.co.nexsoft.shopping_cart.service.OrderService;

@RestController
@RequestMapping("api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public Iterable<Order> getOrders() {
        if (orderService == null) {
            return null;
        } else {
            return orderService.getOrders();
        }
    }
}
