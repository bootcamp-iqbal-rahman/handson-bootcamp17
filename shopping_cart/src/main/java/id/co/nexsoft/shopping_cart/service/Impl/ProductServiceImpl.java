package id.co.nexsoft.shopping_cart.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.shopping_cart.model.Product;
import id.co.nexsoft.shopping_cart.repository.ProductRepository;
import id.co.nexsoft.shopping_cart.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Iterable<Product> getProducts() {
        return productRepository.findAll();
    }

    
}
