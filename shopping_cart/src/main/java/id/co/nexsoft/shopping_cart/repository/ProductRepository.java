package id.co.nexsoft.shopping_cart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.shopping_cart.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}
