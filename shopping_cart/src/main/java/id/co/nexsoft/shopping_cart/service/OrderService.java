package id.co.nexsoft.shopping_cart.service;

import java.util.List;

import id.co.nexsoft.shopping_cart.model.Order;

public interface OrderService {
    Iterable<Order> getOrders();
}
