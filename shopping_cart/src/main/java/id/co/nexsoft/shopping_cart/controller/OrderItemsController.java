package id.co.nexsoft.shopping_cart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.shopping_cart.model.OrderItems;
import id.co.nexsoft.shopping_cart.service.OrderItemsService;

@RestController
@RequestMapping("api/orderitems")
public class OrderItemsController {
    @Autowired
    private OrderItemsService orderItemsService;

    @GetMapping
    public Iterable<OrderItems> getOrderItems() {
        return orderItemsService.getOrderItems();
    }
    
}
