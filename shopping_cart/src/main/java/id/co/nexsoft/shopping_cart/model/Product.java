package id.co.nexsoft.shopping_cart.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String article_number;
    private String description;
    private double purchase_price;
    private double selling_price;
    private String image;
    private Date expired_date;
    private double tax;

    @OneToMany(mappedBy = "product")
    @JsonManagedReference(value = "product-orderitems")
    private List<OrderItems> orderItems;

    public void setSelling_price(double selling_price) {
        this.selling_price = selling_price + (purchase_price * 0.1);
    }
   

}
