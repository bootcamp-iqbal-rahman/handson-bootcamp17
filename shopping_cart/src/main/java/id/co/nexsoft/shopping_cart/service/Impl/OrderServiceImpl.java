package id.co.nexsoft.shopping_cart.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.shopping_cart.model.Order;
import id.co.nexsoft.shopping_cart.repository.OrderRepository;
import id.co.nexsoft.shopping_cart.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Override
    public Iterable<Order> getOrders() {
        return orderRepository.findAll();
    }

    
}
