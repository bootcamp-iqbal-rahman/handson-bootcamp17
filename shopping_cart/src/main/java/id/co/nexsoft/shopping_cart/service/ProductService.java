package id.co.nexsoft.shopping_cart.service;

import id.co.nexsoft.shopping_cart.model.Product;

public interface ProductService {
    Iterable<Product> getProducts();
}
