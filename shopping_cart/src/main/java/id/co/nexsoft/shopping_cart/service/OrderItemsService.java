package id.co.nexsoft.shopping_cart.service;

import id.co.nexsoft.shopping_cart.model.OrderItems;

public interface OrderItemsService {
    Iterable<OrderItems> getOrderItems();
}
