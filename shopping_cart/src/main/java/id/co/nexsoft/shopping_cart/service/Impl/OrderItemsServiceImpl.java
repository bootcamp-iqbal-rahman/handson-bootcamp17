package id.co.nexsoft.shopping_cart.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.shopping_cart.model.OrderItems;
import id.co.nexsoft.shopping_cart.repository.OrderItemsRepository;
import id.co.nexsoft.shopping_cart.service.OrderItemsService;

@Service
public class OrderItemsServiceImpl implements OrderItemsService {
    @Autowired
    private OrderItemsRepository orderItemsRepository;

    @Override
    public Iterable<OrderItems> getOrderItems() {
        return orderItemsRepository.findAll();
    }
    
}
