package id.co.nexsoft.shopping_cart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.shopping_cart.model.Product;
import id.co.nexsoft.shopping_cart.service.Impl.ProductServiceImpl;

@RestController
@RequestMapping("api/products")
public class ProductController {

    @Autowired
    private ProductServiceImpl productService;

    @GetMapping
    public Iterable<Product> getProducts() {
        return productService.getProducts();
    }

}
