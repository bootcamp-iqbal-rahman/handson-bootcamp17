package id.co.nexsoft.shopping_cart.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date order_date;
    private String cashier;

    @Column(unique = true)
    private String order_number;

    private int branch_id;
    private double total_price;
    private double tax;
    private String status;
    private String payment_status;

    @ManyToOne
    @JoinColumn(name = "order_item_id")
    @JsonBackReference(value = "order-items")
    private OrderItems orderItems;

}
