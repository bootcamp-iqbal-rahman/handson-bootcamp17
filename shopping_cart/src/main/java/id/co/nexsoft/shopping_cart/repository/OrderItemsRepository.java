package id.co.nexsoft.shopping_cart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.shopping_cart.model.OrderItems;

public interface OrderItemsRepository extends JpaRepository<OrderItems, Integer> {


}
